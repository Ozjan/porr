This project contains two minimizing algorithms - Filefly Algorithm (FA) and Glow Swarm Optimization (GSO), as well as four different ways of parallelising them.

My (Wiktor Targosiński) work was entire CUDA part, as well as some contribution to others (sequence, OpenACC, OpenMP)

Compile scripts are in 'bin' folder.

Program arguments:
1. function name
2. number of threads ----- !!! Not present in CUDA and OpenACC !!!
3. dimension
4. number of "agents"
5. maximum number of iterations
6. end condition (distance)
7. end condition (function value)
7. maximum nubmer of iterations, where best agent don't change
8. debug print

Run example:
./fa fun1 8 10 20 500 0.5 0.5 500 false
