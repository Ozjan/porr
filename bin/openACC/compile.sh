nvc++ -acc -cudalib=curand -c ../../src/OpenACC/common.cpp
nvc++ -acc -cudalib=curand -c ../../src/OpenACC/FA2.cpp
nvc++ -acc -cudalib=curand -c ../../src/OpenACC/GSO.cpp

nvc++ -acc -cudalib=curand -o fa.out FA2.o common.o 
nvc++ -acc -cudalib=curand -o gso.out common.o GSO.o
