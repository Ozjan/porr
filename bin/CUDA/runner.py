import os
import numpy as np

# compile_fa = os.popen('g++ -fopenmp -lpthread src/OpenMP_3/FA_parallel.cpp src/common.cpp -o fa')

# print(compile_fa.read())

file1 = open("../../results/FA_fun1_CUDA.txt", "w")

runs = {
    './fa fun1 2 4 10000 0.3 0.005 500 false': 10,
    './fa fun1 10 20 10000 0.3 0.005 500 false': 10,
    './fa fun1 20 40 10000 0.5 0.015 500 false': 10,
    './fa fun1 50 100 10000 1 0.055 500 false': 10
}

for run_command in runs:
    print(run_command)
    time_measurements = []
    for i in range(0, runs[run_command]):
        stream = os.popen(run_command)
        output = stream.read()
        measured_time = float(output.split('time: ')[1].split(' seconds')[0])
        time_measurements.append(measured_time)
        # file1.write(output)
    summary_time = sum(time_measurements)
    avg_time = summary_time / len(time_measurements)
    max_time = max(time_measurements)
    min_time = min(time_measurements)
    std_dev = np.std(time_measurements)
    line = f'{run_command} runs: {runs[run_command]} sum: {summary_time}s avg: {avg_time}s max: {max_time}s min: {min_time}s, std dev: {std_dev}s'
    print(line)
    file1.write(line + '\n')
file1.close()

