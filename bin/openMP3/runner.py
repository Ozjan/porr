import os

# compile_fa = os.popen('g++ -fopenmp -lpthread src/OpenMP_3/FA_parallel.cpp src/common.cpp -o fa')

# print(compile_fa.read())

file1 = open("../../results/GSO_fun2_MP3_8u.txt", "w")

runs = {
    './gso fun2 8 2 10 10000 0.1 0.01 500 false': 10,
    './gso fun2 8 10 50 10000 0.5 0.05 500 false': 10,
    './gso fun2 8 20 100 10000 2 0.1 500 false': 10,
    './gso fun2 8 50 250 10000 5 0.5 500 false': 10
}

for run_command in runs:
    print(run_command)
    time_measurements = []
    for i in range(0, runs[run_command]):
        stream = os.popen(run_command)
        output = stream.read()
        measured_time = float(output.split('time: ')[1].split(' seconds')[0])
        time_measurements.append(measured_time)
        # file1.write(output)
    summary_time = sum(time_measurements)
    avg_time = summary_time / len(time_measurements)
    max_time = max(time_measurements)
    min_time = min(time_measurements)
    line = f'{run_command} runs: {runs[run_command]} sum: {summary_time}s avg: {avg_time}s max: {max_time}s min: {min_time}s'
    print(line)
    file1.write(line + '\n')
file1.close()

