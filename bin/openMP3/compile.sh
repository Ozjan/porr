g++ -c ../../src/common.cpp
g++ -c ../../src/OpenMP_3/FA2_parallel.cpp -fopenmp
g++ -c ../../src/OpenMP_3/GSO_parallel.cpp -fopenmp
g++ -o fa common.o FA2_parallel.out -fopenmp -lpthread
g++ -o gso common.o GSO_parallel.out -fopenmp -lpthread