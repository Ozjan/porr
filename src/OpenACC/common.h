#pragma once
#include <math.h>
#include <iostream>
#include <openacc.h>
#include <curand.h>
#include <curand_kernel.h>

typedef double (*scrFunc)(double *, int);

// const int dim = 20; // parametr "n" zadania: 2, 10, 20, 50, 100
// const int population_num = dim * 5; // wielkość populacji
extern const double MIN_RAND;
extern const double MAX_RAND;

extern const int lb_fun1; // lower bound zad1
extern const int ub_fun1;  // upper bound zad1
extern const int lb_fun2; // lower bound zad2
extern const int ub_fun2;  // upper bound zad2

extern double ub;
extern double lb;
extern int dim;
extern int num_threads;
extern int population_num;
extern double end_condition;
extern double end_condition_fun_value;
extern bool debug_print;
extern int max_iters_no_change;
extern int max_it;
extern double** ffa;   // firefly agents
extern double* I;      // light brightness

extern double** gwa;
extern double* L;      // luciferin
extern double* rd;     // radial distance
extern double** P;     //probability matrix
extern bool** N;     // neighborhood matrix

extern scrFunc optimized_problem;
extern scrFunc score_func;
// const double DEFAULT_END_CONDITION = 0.01 * pow(dim, 1.5);

double thread_local_rand(double min, double max);
double fun1(double* x_vec, int dim);
double fun2(double* x_vec, int dim);
bool end_searching(double* best_xi, double end_condition, double end_condition_fun_value, int dim);
void print_solution(int id, double* xi, double intensity, int it_num, int dim);
void print_solution(int id, double* xi, double intensity, int it_num, int dim, double functionValue);

void parseArgs(int argc, char** argv);
void configure_FA();
void configure_GSO();

void initStates(curandState* states, int seed, int pop, int dim);