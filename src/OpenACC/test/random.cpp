#include <iostream>
#include <curand.h>
#include <curand_kernel.h>
#include "openacc.h"
#include "defs.h"
#include <chrono>

using namespace std;

// int n;
// double **x;
// curandState *states;
// #pragma acc declare copyin(x)
// #pragma acc declare copyin(n)
// #pragma acc declare copyin(states)

// void initStates(curandState* states)
// {
//     for (int i = 0; i < n; i++)
//         for (int j = 0; j < n; j++)
//             curand_init(1234UL,i*n+j,0,&states[i*n+j]);

//     return;
// }

// void assignRandom(int i, int j)
// {
//     x[i][j] = curand_uniform(&states[i*n+j]);
// }

int main()
{
    
    printf("0\n");
    int n = 1<<6;
    int y[n][n];

    // cudaMallocManaged(&x, n*sizeof(double*));
    // for (int i=0;i<n;++i)
    //     cudaMallocManaged(&x[i], n*sizeof(double));
    // printf("1\n");
    
    // cudaMallocManaged(&states, n*n*sizeof(curandState));
    printf("2\n");
    // initStates(states, n);
    // #pragma acc data create(n,x[0:n][0:n], states[0:n*n])
    
        // #pragma acc host_data use_device(states)
        {
        // #pragma acc kernels
        // initStates(states);

        }
        
        #pragma acc data create(y[0:n][0:n])
        {
            // for (int i = 0; i < n; i++)
            //     for (int j = 0; j < n; j++)
            //         y[i][j] = 0;
            chrono::steady_clock::time_point begin = chrono::steady_clock::now();

        
        
        for (int k = 0; k < 10; k++)
        {
            
            #pragma acc kernels
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++){
                    y[i][j] = 0;
                    // printf("1");
                    
                }
        }
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        double measured_time = chrono::duration_cast<chrono::duration<float>>(end - begin).count();
    printf("%f\n", measured_time);
        }
    

    // 
        
    return 0;
}

// int main(void)
// {
// int size = 100000;
// int array[size];
// #pragma acc parallel
// {
// for (int i=0; i<size; ++i)
// array[i] = 2 * i;
// }
// printf("%d\n", array[12]);
// }


// printf("3\n");
    // #pragma acc data create(x[0:n][0:n])
    // chrono::steady_clock::time_point begin = chrono::steady_clock::now();



        
        

    
    // chrono::steady_clock::time_point end = chrono::steady_clock::now();
    // double measured_time = chrono::duration_cast<chrono::duration<float>>(end - begin).count();

    // cout << "Searching ended" << endl;
    // cout << "Mesaured time: " << measured_time << " seconds\n";