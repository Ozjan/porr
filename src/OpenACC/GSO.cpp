#include "common.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <string.h>
#include <chrono>
#include <openacc.h>
#include <openacc_curand.h>

using namespace std;

double beta_0 = 0.08; // slajd 36 - parametry dla GSO
double rho = 0.4;
double gamma_custom = 0.6;
double s = 1;
double nt = 5.0;
double l_zero = 5.0;
double rs = 250.0;  //the constant radial sensor range
double** gwa;
double* L;      // luciferin
double* rd;     // radial distance
double** P;     //probability matrix
bool** N;     // neighborhood matrix
double ub;
double lb;
int dim;
int population_num;

curandState *RNGstates;

#pragma acc declare copyin(gwa)
#pragma acc declare copyin(dim)
#pragma acc declare copyin(L)
#pragma acc declare copyin(population_num)
#pragma acc declare copyin(P)
#pragma acc declare copyin(nt)
#pragma acc declare copyin(lb)
#pragma acc declare copyin(N)
#pragma acc declare copyin(beta_0)
#pragma acc declare copyin(rd)
#pragma acc declare copyin(rs)
#pragma acc declare copyin(ub)
#pragma acc declare copyin(s)
#pragma acc declare copyin(l_zero)
#pragma acc declare copyin(RNGstates)

void CUDAinit()
{

    

    // alokowanie w pamieci tablic zmiennych, modyfikowanych w kernelach
    cudaMallocManaged(&gwa, population_num*sizeof(double*));
    for (int i=0;i<population_num;++i)
        cudaMallocManaged(&gwa[i],dim*sizeof(double));

    cudaMallocManaged(&L, population_num*sizeof(double));
    cudaMallocManaged(&rd, population_num*sizeof(double));

    cudaMallocManaged(&P, population_num*sizeof(double*));
    for (int i=0;i<population_num;++i)
        cudaMallocManaged(&P[i],population_num*sizeof(double));
    cudaMallocManaged(&N, population_num*sizeof(bool*));
    for (int i=0;i<population_num;++i)
        cudaMallocManaged(&N[i],population_num*sizeof(bool));

    // inicjowanie stanow generatora liczb losowych
    cudaMallocManaged(&RNGstates, population_num*population_num*sizeof(curandState));
    int ra = thread_local_rand(0,10000000);
    initStates(RNGstates, ra, population_num, dim);
}

// // optimization tasks
// double fun1(double* x_vec, int dim)
// {
//     double sum = 0.0;
//     double product = 1.0;
//     #pragma acc loop reduction(+:sum,*:product)
//     for (int i = 0; i < dim; i++)
//     {
//         sum += pow(x_vec[i], 2);
//         product *= cos(x_vec[i] / (i + 1));
//     }
//     return sum / 40 + 1 - product;
    
// }

// double fun2(double* x_vec, int dim)
// {
//     double sum1 = 0.0;
//     double sum2 = 0.0;

//     #pragma acc loop reduction(+:sum1,sum2)
//     for (int i = 0; i < dim; i++)
//     {
//         sum1 += pow(x_vec[i], 2);
//         sum2 += cos(2 * M_PI * x_vec[i]);
//     }
//     return -20 * exp(-0.2 * sqrt(sum1 / dim )) - exp(sum2 / dim ) + 20 + M_E;
// }

double d_distance(int i, int j)
{
    double sum = 0.0;
    
    for (int xi = 0; xi < dim; xi++)
    {
        sum += pow(gwa[i][xi] - gwa[j][xi], 2);
    }
    return sqrt(sum);
}

void init_gw_population(int lb, int ub)
{
    // #pragma acc kernelss
    for (int i = 0; i < population_num; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            double r = curand_uniform(&RNGstates[0]);
            gwa[i][j] = r * (ub - lb) + lb;
        }
        L[i] = l_zero;  // Początkowa wielkość lucyferyny
        rd[i] = rs; // Początkowy dystans radialny
    }
}

int current_best_id()
{
    int current_best = 0;
    for (int i = 0; i < population_num; i++)
    {
        if (L[i] > L[current_best])
        {
            current_best = i;
        }
    }
    // cout << "Best luciferin " << L[current_best] << endl;
    return current_best;
}

void update_luciferin(scrFunc optimized_problem, int iter)
{
    #pragma acc parallel loop
    for (int k = 0; k < population_num; k++)
    {
        L[k] = (1.0 - rho) * L[k] + gamma_custom * score_func(gwa[k],dim);
    }
}

void set_neighborhood(int i)
{
    N[i][i] = 0;
    for (int j = 0; j < population_num; j++)
    {
        if (j != i)
        {
            if ((L[j] > L[i]) && (d_distance(i, j) < rd[i]))
                N[i][j] = 1;
            else
                N[i][j] = 0;
        }
    }
}

int count_neighbors(int i)
{
    int neighbors = 0;
    for (int j = 0; j < population_num; j++)
        {
            if (N[i][j] == 1){
                neighbors++;
            }
        }

    return neighbors;
}

void probability_fun(int i)
{
    double sum = 0;
    for (int j = 0; j < population_num; j++)
    {
        sum += N[i][j] * max(0.0,(L[j] - L[i]));
    }

    for (int j = 0; j < population_num; j++)
    {
        if(L[j] - L[i] < 0 || sum == 0){
            P[i][j] = 0;
        } else {
            P[i][j] = N[i][j] * (L[j] - L[i]) / sum;
        }
        //cout << "Probability " << P[i][j];
    }
}

int select_glowworm(int i)
{
    double rndNr = curand_uniform(&RNGstates[i*population_num]);
    double offset = 0.0;

    for (int j = 0; j < population_num; j++)
    {
        offset += P[i][j];
        if(rndNr < offset){
            return j;
        }
    }
    // if(offset != 0)
    //     cout << "Mistake! Mimo sąsiadów nie przesunięto świetlika!" << endl;
    return i;
}

double clip_x(double x) {
    return min(max(x, lb), ub);
}

void move_glowworm(int i, int j)
{
    if(i == j)
        return;

    for (int xi = 0; xi < dim; xi++)
    {
        double var = gwa[j][xi] - gwa[i][xi];
        gwa[i][xi] = gwa[i][xi] + s * (var / d_distance(i,j));
    }
}

double clip_solution(double solution)
{
    return min(rs, max(0.0, solution));
}

// x =|Ni(t)| is the actual number of neighbors
void update_rd(int i)
{
    rd[i] = clip_solution(rd[i] + beta_0 * (nt - count_neighbors(i)));
}

void runGSO() {
    int it_num = 0;
    int best_id = 0;

    // for (int i = 0; i < population_num; i++)
    // {
    //     gwa[i] = new double[dim];  
    //     P[i] = new double[population_num]; 
    //     N[i] = new bool[population_num];    
    // }
    #pragma acc kernels
    init_gw_population(lb, ub);

    //double time_start = omp_get_wtime();
    double current_best_func_val = 0.0d;
    double last_changed = 0.0d;
    int last_change_it_num = it_num;
    chrono::steady_clock::time_point begin = chrono::steady_clock::now();
    {
    while (it_num < max_it)
    {

        // #pragma acc kernels
        update_luciferin(optimized_problem, it_num);

        
        best_id = current_best_id();
        
        // #pragma acc parallel
        for (int k = 0; k < population_num; k++)
        {
            // określanie zbioru sąsiadów
            set_neighborhood(k);
            
            // obliczanie prawdopodobieństwa przemieszczenia agenta w kierunku sąsiada j
            probability_fun(k);
            int j = select_glowworm(k);
            move_glowworm(k,j);

            // modyfikacja zakresu radialnego sąsiedztwa
            update_rd(k);
        }
        if (end_searching(gwa[best_id], end_condition, end_condition_fun_value, dim) || it_num - last_change_it_num > max_iters_no_change)
            break;
        current_best_func_val = optimized_problem(gwa[best_id], dim);
        if (current_best_func_val != last_changed) {
            last_change_it_num = it_num;
            last_changed = current_best_func_val;
        }

        if (it_num % 10 == 0 && debug_print)
        {
            print_solution(best_id, gwa[best_id], L[best_id], it_num, dim, optimized_problem(gwa[best_id], dim));
        }
        it_num++;
    }

    //double measured_time = omp_get_wtime() - time_start;
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    double measured_time = chrono::duration_cast<chrono::duration<float>>(end - begin).count();
    if (debug_print)
    {
        print_solution(best_id, gwa[best_id], L[best_id], it_num, dim, optimized_problem(gwa[best_id], dim));
    }
    
    cout << "Searching ended" << endl;
    cout << "Mesaured time: " << measured_time << " seconds" << endl;
}}

int main(int argc, char** argv)
{
    parseArgs(argc, argv);
    configure_GSO();
    CUDAinit();
    //acc_set_device_type(acc_device_t(5)); //nvidia
    
    runGSO();

    return 0;
}