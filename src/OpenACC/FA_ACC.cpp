#include "common_ACC.h"
#include <iostream>
#include <openacc.h>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <string.h>
#include <chrono>

using namespace std;


double beta0 = 0.7; // beta parameter
double alpha = 0.6;
double gammaParam = 1; // gamma parameter
double search_range = 1; // ub_fun1 - lb_fun1;

double clip_solution(double solution)
{
    return max(min(solution, ub), lb);
}

void update_intensities(scrFunc optimized_problem)
{
    int k;
      
    #pragma acc kernels
    for (k = 0; k < population_num; k++){
        I[k] = 1.0 / optimized_problem(ffa[k], dim);
    }
 
}

double distance(int i, int j)
{
    double sum = 0.0;
    int xi;

        for (xi = 0; xi < dim; xi++)
        {
            sum += pow(ffa[i][xi] - ffa[j][xi], 2);
        }

    return sqrt(sum);
}

void update_solution_for_pulled(int pulled, int pulled_by, scrFunc optimized_problem)
{
    double dist = distance(pulled, pulled_by);
    double beta = beta0 * exp(-gammaParam * pow(dist, 2));
    double *tmp = new double[dim];
    int dimension;

    for (dimension = 0; dimension < dim; dimension++)
    {
        double a1 = ffa[pulled][dimension] + beta * (ffa[pulled_by][dimension] - ffa[pulled][dimension]);
        double a2 = alpha * (thread_local_rand(MIN_RAND, MAX_RAND));
        tmp[dimension] = clip_solution(a1 + a2);
    }
    
    if (1.0 / optimized_problem(tmp, dim) > I[pulled])
    {
        int dimension;
        for (dimension = 0; dimension < dim; dimension++)
        {
            ffa[pulled][dimension] = tmp[dimension];
        }
    }
    
    delete tmp;
}

void update_solution_for_best(int best, scrFunc optimized_problem)
{
    double *tmp = new double[dim];
    int dimension;
        
        #pragma acc kernels
        for (dimension = 0; dimension < dim; dimension++)
        {
            tmp[dimension] = clip_solution(ffa[best][dimension] + alpha * thread_local_rand(MIN_RAND, MAX_RAND));
        }

    if (1.0 / optimized_problem(tmp, dim) > I[best])
    {
        dimension=0;

        #pragma acc kernels
        for (dimension = 0; dimension < dim; dimension++)
        {
            ffa[best][dimension] = tmp[dimension];
        }
    }
    delete tmp;
}

void init_fa_population(double lb, double ub)
{
    for (int i = 0; i < population_num; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            double r = thread_local_rand(0, 1);
            ffa[i][j] = r * (ub - lb) + lb;
        }
        I[i] = 1.0;
    }
}

int current_best_id()
{
        int current_best = 0;
        int i;
        for (i = 0; i < population_num; i++)
        {
            if (I[i] > I[current_best]) {
                current_best += i - current_best;
            }
        }
    return current_best;
}

void runFA() {
    int best_id = 0;
    int it_num = 0;

    for (int i = 0; i < population_num; i++)
    {
        ffa[i] = new double[dim];
    }
    init_fa_population(lb, ub);

    int current_best = 0;
    // double time_start = omp_get_wtime();
    chrono::steady_clock::time_point begin = chrono::steady_clock::now();
    double current_best_func_val = 0.0d;
    double last_changed = 0.0d;
    int last_change_it_num = it_num;
    while (it_num < max_it)
    {
        update_intensities(optimized_problem);
        best_id = current_best_id();
        if (end_searching(ffa[best_id], end_condition, dim) || it_num - last_change_it_num > max_iters_no_change)
            break;

        current_best_func_val = optimized_problem(ffa[best_id], dim);
        if (current_best_func_val != last_changed) {
            last_change_it_num = it_num;
            last_changed = current_best_func_val;
        }
        int i,j;
        #pragma acc kernels
        //#pragma omp parallel for private(i,j) shared(population_num, I, optimized_problem)
            for (i = 0; i < population_num; i++)
            {
                // #pragma omp parallel for private(j) shared(population_num, I, i, it_num, optimized_problem)
                for (j = 0; j < population_num; j++)
                {
                    if (I[j] > I[i]) {
                        update_solution_for_pulled(i, j, optimized_problem);
                    }
                }
            }
        
        update_solution_for_best(best_id, optimized_problem);
        if (it_num % 10 == 0 && debug_print)
        {
            print_solution(best_id, ffa[best_id], I[best_id], it_num, dim, optimized_problem(ffa[best_id], dim));
        }
        it_num++;
    }

    //double measured_time = omp_get_wtime() - time_start;
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    double measured_time = chrono::duration_cast<chrono::duration<float>>(end - begin).count();
    if (debug_print) {
        print_solution(best_id, ffa[best_id], I[best_id], it_num, dim, optimized_problem(ffa[best_id], dim));
    }
    cout << "Searching ended" << endl;
    cout << "Mesaured time: " << measured_time << " seconds";
}

int main(int argc, char** argv)
{
    parseArgs(argc, argv);
    configure_FA();
    //acc_set_device_type(acc_device_t(5)); //nvidia
 
    runFA();
    return 0;
}
