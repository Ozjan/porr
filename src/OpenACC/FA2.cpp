#include "common.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <string.h>
#include <chrono>

using namespace std;


double beta0 = 0.3; // beta parameter
double alpha = 0.7;
double gammaParam = 1; // gamma parameter
double search_range = 1; // ub_fun1 - lb_fun1;

double** ffa;   // firefly agents
double* I;      // light brightness
double ub;
double lb;
int dim;
int population_num;

// variables for RNG
curandState *RNGstates;
double **temp_ffa;

#pragma acc declare copyin(ffa)
#pragma acc declare copyin(temp_ffa)
#pragma acc declare copyin(I)
#pragma acc declare copyin(dim)
#pragma acc declare copyin(population_num)
#pragma acc declare copyin(ub)
#pragma acc declare copyin(lb)
#pragma acc declare copyin(gammaParam)
#pragma acc declare copyin(beta0)
#pragma acc declare copyin(alpha)
#pragma acc declare copyin(RNGstates)



void CUDAinit()
{
    cudaMallocManaged(&I, population_num*sizeof(double));
    cudaMallocManaged(&ffa, population_num*sizeof(double*));
    cudaMallocManaged(&temp_ffa, population_num*sizeof(double*));
    for (int i=0;i<population_num;++i)
    {
        cudaMallocManaged(&ffa[i], dim*sizeof(double));
        cudaMallocManaged(&temp_ffa[i], dim*sizeof(double));
    }

    cudaMallocManaged(&RNGstates, population_num*dim*sizeof(curandState));

    int ra = thread_local_rand(0,10000000);
    initStates(RNGstates, ra, population_num, dim);
}

// // optimization tasks
// double fun1(double* x_vec, int dim)
// {
//     double sum = 0.0;
//     double product = 1.0;

//     #pragma acc loop reduction(+:sum,*:product)
//     for (int i = 0; i < dim; i++)
//     {
//         sum += pow(x_vec[i], 2);
//         product *= cos(x_vec[i] / (i + 1));
//     }
//     return sum / 40 + 1 - product;
    
// }

// double fun2(double* x_vec, int dim)
// {
//     double sum1 = 0.0;
//     double sum2 = 0.0;

//     #pragma acc loop reduction(+:sum1,sum2)
//     for (int i = 0; i < dim; i++)
//     {
//         sum1 += pow(x_vec[i], 2);
//         sum2 += cos(2 * M_PI * x_vec[i]);
//     }
//     return -20 * exp(-0.2 * sqrt(sum1 / dim )) - exp(sum2 / dim ) + 20 + M_E;
// }

double clip_solution(double solution)
{
    return max(min(solution, ub), lb);
}

void update_intensities(scrFunc optimized_problem)
{
      
    #pragma acc parallel loop
    for (int k = 0; k < population_num; k++){
        I[k] = 1.0 / optimized_problem(ffa[k], dim);
    }
 
}

double distance(int i, int j)
{
    double sum = 0.0;
    #pragma acc loop reduction(+:sum)
    for (int xi = 0; xi < dim; xi++)
    {
        sum += pow(ffa[i][xi] - ffa[j][xi], 2);
    }

    return sqrt(sum);
}

void move_randomly(int agent, scrFunc optimized_problem)
{
    #pragma acc loop
    for (int dimension = 0; dimension < dim; dimension++)
    {
        temp_ffa[agent][dimension] = ffa[agent][dimension] + alpha * (curand_uniform(&RNGstates[agent*population_num+dimension])-0.5);
    }

    if (1.0 / optimized_problem(temp_ffa[agent], dim) > I[agent])
    {
        #pragma acc loop
        for (int dimension = 0; dimension < dim; dimension++)
        {
            ffa[agent][dimension] = temp_ffa[agent][dimension];
        }
    }
}

void update_solution_for_pulled(int pulled, int pulled_by, scrFunc optimized_problem)
{
    double dist = distance(pulled, pulled_by);
    double beta = beta0 * exp(-gammaParam * pow(dist, 2));
    #pragma acc loop
    for (int dimension = 0; dimension < dim; dimension++)
    {
        ffa[pulled][dimension] += beta * (ffa[pulled_by][dimension] - ffa[pulled][dimension]);
        ffa[pulled][dimension] = clip_solution(ffa[pulled][dimension]);
    }
    
}


void init_fa_population()
{
    // #pragma acc kernels
    for (int i = 0; i < population_num; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            double r = curand_uniform(&RNGstates[i*population_num+j]);
            ffa[i][j] = r * (ub - lb) + lb;
        }
        I[i] = 1.0;
    }
}

int current_best_id()
{
    int current_best = 0;
    // #pragma acc parallel loop seq
    for (int i = 0; i < population_num; i++)
    {
        if (I[i] > I[current_best]) {
            current_best = i;
        }
    }
    return current_best;
}

void runFA() {
    int best_id = 0;
    int it_num = 0;

    // #pragma acc data create(RNGstates[0:population_num*dim])
    init_fa_population();

    // double time_start = omp_get_wtime();
    double current_best_func_val = 0.0d;
    double last_changed = 0.0d;
    int last_change_it_num = it_num;
    // #pragma acc data create(dim,ffa[0:population_num][0:dim])
    

    chrono::steady_clock::time_point begin = chrono::steady_clock::now();
    while (it_num < max_it)
    {
        update_intensities(optimized_problem);
        best_id = current_best_id();
        if (end_searching(ffa[best_id], end_condition, end_condition_fun_value, dim) || it_num - last_change_it_num > max_iters_no_change)
            break;

        current_best_func_val = optimized_problem(ffa[best_id], dim);
        if (current_best_func_val != last_changed) {
            last_change_it_num = it_num;
            last_changed = current_best_func_val;
        }
        #pragma acc parallel loop
        for (int i = 0; i < population_num; i++)
        {
            #pragma acc loop
            for (int j = 0; j < population_num; j++)
            {
                if (I[j] > I[i]) {
                    update_solution_for_pulled(i, j, optimized_problem);
                }
            }
            move_randomly(i, optimized_problem);
            
        }

        
        
        if (it_num % 10 == 0 && debug_print)
        {
            print_solution(best_id, ffa[best_id], I[best_id], it_num, dim, optimized_problem(ffa[best_id], dim));
        }
        it_num++;
    }

    //double measured_time = omp_get_wtime() - time_start;
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    double measured_time = chrono::duration_cast<chrono::duration<float>>(end - begin).count();
    if (debug_print) {
        print_solution(best_id, ffa[best_id], I[best_id], it_num, dim, optimized_problem(ffa[best_id], dim));
    }
    cout << "Searching ended" << endl;
    cout << "Mesaured time: " << measured_time << " seconds\n";
}

int main(int argc, char** argv)
{
    parseArgs(argc, argv);
    CUDAinit();
    // configure_FA();
    //acc_set_device_type(acc_device_t(5)); //nvidia
 
    runFA();
    return 0;
}
