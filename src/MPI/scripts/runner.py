import os
import time
import argparse
import numpy as np


parser = argparse.ArgumentParser(
                    prog = 'Runner',
                    description = 'Runs GSO/FA',
                    epilog = 'eeee')
parser.add_argument('-a', '--algorithm', required=False, default='FA')
args = parser.parse_args()

algorithm = args.algorithm.upper()

result_file = open(f'../results/{algorithm}_results.xlsx', "w")

exec_file = f'./{algorithm}.out'

fun_name = 'fun1'

# Program arguments:
# 1. function name
# 2. synchronize each n iters
# 3. dimension
# 4. number of "agents"
# 5. maximum number of iterations
# 6. end condition 
# 7. end condition function value diff
# 8. maximum nubmer of iterations, where best agent don't change
# 9. debug print

end_condition_location = 0.1
end_condition_function_value = 0.1
run_n_times = 10
max_iters_no_change = 1000
runs = {
    # f'mpirun -n 1 {exec_file} {fun_name} 10 2 12 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
    # f'mpirun -n 4 {exec_file} {fun_name} 10 2 12 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
    # f'mpirun -n 1 {exec_file} {fun_name} 1 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
    # f'mpirun -n 2 {exec_file} {fun_name} 1 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
    # f'mpirun -n 4 {exec_file} {fun_name} 1 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
    # f'mpirun -n 8 {exec_file} {fun_name} 1 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
    # f'mpirun -n 16 {exec_file} {fun_name} 1 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times,
    # f'mpirun -n 1 {exec_file} {fun_name} 10 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
    # f'mpirun -n 2 {exec_file} {fun_name} 10 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
    # f'mpirun -n 4 {exec_file} {fun_name} 10 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
    # f'mpirun -n 8 {exec_file} {fun_name} 10 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
    # f'mpirun -n 16 {exec_file} {fun_name} 10 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times,
    # f'mpirun --oversubscribe -c 1 {exec_file} {fun_name} 10 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} true':    run_n_times, 
    f'mpirun --oversubscribe -c 2 {exec_file} {fun_name} 10 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} true':    run_n_times, 
    f'mpirun --oversubscribe -c 4 {exec_file} {fun_name} 10 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} true':    run_n_times, 
    f'mpirun --oversubscribe -c 8 {exec_file} {fun_name} 10 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} true':    run_n_times, 
    # f'mpirun --oversubscribe -c 16 {exec_file} {fun_name} 10 10 48 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} true':    run_n_times,
#     f'mpirun -n 1 {exec_file} {fun_name} 10 20 96 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
#     f'mpirun -n 2 {exec_file} {fun_name} 10 20 96 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
#     f'mpirun -n 4 {exec_file} {fun_name} 10 20 96 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
#     f'mpirun -n 8 {exec_file} {fun_name} 10 20 96 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
#     f'mpirun -n 16 {exec_file} {fun_name} 10 20 96 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
#     f'mpirun -n 1 {exec_file} {fun_name} 10 30 144 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
#     f'mpirun -n 2 {exec_file} {fun_name} 10 30 144 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
#     f'mpirun -n 4 {exec_file} {fun_name} 10 30 144 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
#     f'mpirun -n 8 {exec_file} {fun_name} 10 30 144 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times, 
#     f'mpirun -n 16 {exec_file} {fun_name} 10 30 144 10000 {end_condition_location} {end_condition_function_value} {max_iters_no_change} false':    run_n_times,
}
    
header_row = f'command,run count, sum[s], avg[s], max[s], min[s], stdev[s]'
result_file.write(header_row + '\n')
for run_command in runs:
    print(run_command)
    time_measurements = []
    for i in range(0, runs[run_command]):
        stream = os.popen(run_command)
        output = stream.read()    
        print(output)
        measured_time = float(output.split(' in: ')[1].split(' s')[0])
        time_measurements.append(measured_time)
    avg_time = np.average(time_measurements)
    std_dev = np.std(time_measurements)
    filtered_measurements = [t for t in time_measurements if abs(avg_time - t)<2*std_dev]
    summary_time = np.sum(filtered_measurements)
    avg_time = np.average(filtered_measurements)
    max_time = max(filtered_measurements)
    min_time = min(filtered_measurements)
    std_dev = np.std(filtered_measurements)
    
    row = f'{run_command},{runs[run_command]},{summary_time},{avg_time},{max_time},{min_time},{std_dev}' 
    line = f'{run_command} runs: {runs[run_command]} sum: {summary_time}s avg: {avg_time}s max: {max_time}s min: {min_time}s, std dev: {std_dev}s'
    print(line)
    result_file.write(row + '\n')
result_file.close()











