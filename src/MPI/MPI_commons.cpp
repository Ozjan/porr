#include "MPI_commons.h"
#include "common.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <string.h>
#include <chrono>
#include <unistd.h>
#include "mpi.h"

using namespace std;

int size_per_proc;
MPI_Status status;
MPI_Request request;
int mynum;
int from;
int to;
int info;
int nprocs;

void initMPI(int argc, char** argv) {
    MPI_Init( & argc, & argv);
    MPI_Comm_rank(MPI_COMM_WORLD, & mynum);
    MPI_Comm_size(MPI_COMM_WORLD, & nprocs);
    size_per_proc = round (population_num / (double) nprocs);
	from = size_per_proc * (mynum);
	to = size_per_proc * (mynum + 1);
}

void print_population(double** population) {
    cout << "My num: " <<  mynum << endl;
    for (int j=0; j < population_num; j++) {
        cout << "Population for: " << j << " mynum: " << mynum << ": ";
        for (int k = 0; k < dim; k++) {
            cout << population[j][k] << " ";
        }
        cout << endl;
    }
}

void slep() {
    if (mynum == 0) {
        sleep(1);
    } 
}

double* flatten_2d_array(double** toFlatten, int from, int to, int dim) {
    double *tmp = new double[(to - from) * dim];
    for (int i = 0; i < (to - from); i++) {
        for (int j = 0; j < dim; j++) {
            tmp[i * dim + j] = toFlatten[i + from][j];
        }
    }
    return tmp;
}

void bcast_my_population(double** locationBuffer, double* scoreBuffer) {
    double *sendBuffer = flatten_2d_array(locationBuffer, from, to, dim);
    for (int i = 0; i < nprocs; i++) {
        if (i != mynum) {
            info = MPI_Isend( & (sendBuffer[0]), size_per_proc * dim, MPI_DOUBLE, i, 2, MPI_COMM_WORLD, & request);
            info = MPI_Isend( & (scoreBuffer[from]), size_per_proc, MPI_DOUBLE, i, 2, MPI_COMM_WORLD, & request);
        }
    }
}

void blocking_recv(double *locationRecvBuffer, double** locationBuffer, double* scoreBuffer, int source, int from) {
    info = MPI_Recv( & (locationRecvBuffer[0]), size_per_proc*dim, MPI_DOUBLE, source, 2, MPI_COMM_WORLD, & status);
    info = MPI_Recv( & (scoreBuffer[from]), size_per_proc, MPI_DOUBLE, source, 2, MPI_COMM_WORLD, & status);
    for (int j = 0; j < size_per_proc; ++j) {
        for (int k = 0; k < dim; ++k) {
            locationBuffer[j + from][k] = locationRecvBuffer[j * dim + k];
        }
    }
}

void recv_others_population(double** locationBuffer, double* scoreBuffer, bool blocking) {
    double *locationRecvBuffer = new double[size_per_proc * dim];
    int flag = 0;
    for (int i = 0; i < nprocs; i++) {
        if (i != mynum) {
            int from_new = i * size_per_proc;
            int to_new = (i + 1) * size_per_proc;
            int source = i;
            if (blocking) {
                blocking_recv(locationRecvBuffer, locationBuffer, scoreBuffer, source, from_new);
            } else {
                MPI_Iprobe(i, 2, MPI_COMM_WORLD, &flag, &status);
                if (flag > 0) {
                    blocking_recv(locationRecvBuffer, locationBuffer, scoreBuffer, source, from_new);
                    flag = 0;
                }
            }
        }
    }
}

void recv_others_population(double** locationBuffer, double* scoreBuffer) {
    recv_others_population(locationBuffer, scoreBuffer, true);
}

void recv_init_population(double** locationBuffer, double *tmpBuffer) {
    for (int j = 0; j < population_num; ++j) {
        for (int k = 0; k < dim; ++k) {
            locationBuffer[j][k] = tmpBuffer[j * dim + k];
        }
    }
}

double exchange_results(int best_id, double** locationBuffer) {
    double best_result = optimized_problem(locationBuffer[best_id], dim);
    if (mynum == 0) {
        // receive results from other processes
		double another_result;
		for (int i = 1; i < nprocs; i++) {
			int source = i;
			info = MPI_Recv( & another_result, 1, MPI_DOUBLE, source, 4, MPI_COMM_WORLD, & status);
		    printf("Root got restult = %f from %d\n", another_result, i);
			if (another_result < best_result) {
                printf("Change current best restult: %f to %f from %d\n", best_result, another_result, i);
				best_result = another_result;
			} else {
                printf("Stay with current resutlt: %f\n", best_result);
			}
		}
		printf("Best found result was: %7.5f\n", best_result);
	} else {
		// send result to process with mynum = 0
		info = MPI_Send( & best_result, 1, MPI_DOUBLE, 0, 4, MPI_COMM_WORLD);
		if (info != 0) {
			printf("Instance %d failed to send its solution. Exiting 1\n", mynum);
			exit(1);
		}
	}
    return best_result;
}

void inform_on_finish(int best_id) {
    for (int i = 0; i < nprocs; i++) {
        if (i != mynum) {
            info = MPI_Isend( &best_id, 1, MPI_INT, i, 3, MPI_COMM_WORLD, & request);
        }
    }
}

int check_for_finish() {
    int flag = 0;
    int last_best_id = -1;
    for (int i = 0; i < nprocs; i++) {
        if (i != mynum) {
            MPI_Iprobe(i, 3, MPI_COMM_WORLD, &flag, &status);
            if (flag > 0) {
                last_best_id = -1;
                MPI_Recv( &last_best_id, 1, MPI_INT, i, 3, MPI_COMM_WORLD, & status);
                flag = 0;
            }
        }
    }
    return last_best_id > population_num ? 0 : last_best_id;
}