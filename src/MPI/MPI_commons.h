#pragma once
#ifndef MPI_COMMONS_H
#define MPI_COMMONS_H
#include "mpi.h"
extern int size_per_proc;
extern MPI_Status status;
extern MPI_Request request;
extern int mynum;
extern int from;
extern int to;
extern int info;
extern int nprocs;

void initMPI(int argc, char** argv);
void print_population(double** population);
void slep();
double* flatten_2d_array(double** toFlatten, int from, int to, int dim);
void inform_on_finish(int best_id);
int check_for_finish();
void bcast_my_population(double** locationBuffer, double* scoreBuffer);
void recv_others_population(double** locationBuffer, double* scoreBuffer, bool blocking);
void recv_others_population(double** locationBuffer, double* scoreBuffer);
double exchange_results(int best_id, double** locationBuffer);
void recv_init_population(double** dest, double *buffer);
#endif