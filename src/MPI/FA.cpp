#include "common.h"
#include "MPI_commons.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <string.h>
#include <chrono>
#include "mpi.h"

using namespace std;

double beta0 = 0.2; // beta parameter
double alpha = 0.7;

int type = 2, nbytes = 0, EUI_SUCCEED = 0;

double clip_solution(double solution)
{
    return max(min(solution, ub), lb);
}

void update_intensities(scrFunc optimized_problem)
{
    int k;   
    for (k = from; k < to; k++){
        I[k] = 1.0 / optimized_problem(ffa[k], dim);
    }
 
}

double distance(int i, int j)
{
    double sum = 0.0;
    for (int xi = 0; xi < dim; xi++)
    {
        sum += pow(ffa[i][xi] - ffa[j][xi], 2);
    }

    return sqrt(sum);
}

void move_randomly(int agent, scrFunc optimized_problem)
{
    double *tmp = new double[dim];
    int dimension;
        
    for (dimension = 0; dimension < dim; dimension++)
    {
        tmp[dimension] = clip_solution(ffa[agent][dimension] + alpha * thread_local_rand(MIN_RAND, MAX_RAND));
    }

    if (1.0 / optimized_problem(tmp, dim) > I[agent])
    {
        dimension=0;

        for (dimension = 0; dimension < dim; dimension++)
        {
            ffa[agent][dimension] = tmp[dimension];
        }
    }
    delete tmp;
}

void update_solution_for_pulled(int pulled, int pulled_by, scrFunc optimized_problem)
{
    double dist = distance(pulled, pulled_by);
    double beta = beta0 * exp(-gammaParam * pow(dist, 2));
    int dimension;

    for (dimension = 0; dimension < dim; dimension++)
    {
        ffa[pulled][dimension] += beta * (ffa[pulled_by][dimension] - ffa[pulled][dimension]);
        ffa[pulled][dimension] = clip_solution(ffa[pulled][dimension]);
    }
}

void init_fa_population(double lb, double ub)
{
    for (int i = 0; i < population_num; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            double r = thread_local_rand(0, 1);
            ffa[i][j] = r * (ub - lb) + lb;
        }
        I[i] = 1.0;
    }
}

int current_best_id() {
    int current_best = 0;
    int i;
    for (i = 0; i < population_num; i++)
    {
        if (I[i] > I[current_best]) {
            current_best = i;
        }
    }
    return current_best;
}

int runFA() 
{
    int best_id = 0;
    int it_num = 0;
    printf("Run fa with params from %d to %d. my ID: %d\n", from, to, mynum);

    double current_best_func_val = 0.0d;
    double last_changed = 0.0d;
    int last_change_it_num = it_num;
    while (it_num < max_it)
    {
        update_intensities(optimized_problem);
        best_id = current_best_id();
        int received_best_id = check_for_finish();
        if (received_best_id >= 0) {
            bcast_my_population(ffa, I);
            recv_others_population(ffa, I, false);
            double received_func_val = optimized_problem(ffa[received_best_id], dim);
            if (current_best_func_val > received_func_val ) {
                best_id = received_best_id;
                current_best_func_val = received_func_val;
            }
            break;
        }
        current_best_func_val = optimized_problem(ffa[best_id], dim);
        if (current_best_func_val != last_changed) {
            last_change_it_num = it_num;
            last_changed = current_best_func_val;
        }
        if (end_searching(ffa[best_id], end_condition, end_condition_fun_value, dim) || it_num - last_change_it_num > max_iters_no_change) {
            inform_on_finish(best_id);
            bcast_my_population(ffa, I);
            recv_others_population(ffa, I, false);
            break;
        }

        int i,j;
        for (i = from; i < to; i++)
        {
            for (j = 0; j < population_num; j++)
            {
                if (I[j] > I[i]) {
                    update_solution_for_pulled(i, j, optimized_problem);
                }
            }
            move_randomly(i, optimized_problem);
        }
        if (it_num > 0 && it_num % iters_without_sync == 0) {
            bcast_my_population(ffa, I);
            recv_others_population(ffa, I, false);
            if (debug_print) {
                print_solution(best_id, ffa[best_id], I[best_id], it_num, dim, optimized_problem(ffa[best_id], dim));
            }
            best_id = current_best_id();
            bool should_end = end_searching(ffa[best_id], end_condition, end_condition_fun_value, dim);
            current_best_func_val = optimized_problem(ffa[best_id], dim);
            if (current_best_func_val != last_changed) {
                last_change_it_num = it_num;
                last_changed = current_best_func_val;
            }
        }
        it_num++;
    }
    if (debug_print) {
        print_solution(best_id, ffa[best_id], I[best_id], it_num, dim, optimized_problem(ffa[best_id], dim));
    }
    printf("Searching ended for process with ID %d with best id %d and its score %f\n", mynum, best_id, current_best_func_val);
    return best_id;
}

double* prepare_population() {
    init_fa_population(lb, ub);
    cout << mynum << ": prepared FA population." << endl;
    return flatten_2d_array(ffa, 0, population_num, dim);
}

int main(int argc, char** argv)
{
    parseArgs(argc, argv);
    // initialization MPI
    initMPI(argc, argv);
    configure_FA();

    double *sendrecv_buffer;
    if (mynum==0){
        // init population in root process
        sendrecv_buffer = prepare_population();
    } else {
        sendrecv_buffer = new double[population_num * dim];
    }
    info = MPI_Bcast(&(sendrecv_buffer[0]),population_num * dim, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    if (mynum != 0) {
        // receive initial population in rest of processes
        recv_init_population(ffa, sendrecv_buffer);
        printf(" Received initial population, my ID: %d\n", mynum) ;
    }

    chrono::steady_clock::time_point begin = chrono::steady_clock::now();
    // GSO run - find best for current process
	int best_id = runFA();
    // exchange solution and print best in root process
    float best_result = exchange_results(best_id, ffa);
    if (mynum == 0) {
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        double measured_time = chrono::duration_cast<chrono::duration<float>>(end - begin).count();
        printf("Found solution %f in: %f s\n", best_result, measured_time);
    }
    MPI_Finalize();
    return 0;
}
