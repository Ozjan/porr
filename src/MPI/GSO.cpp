#include "common.h"
#include "MPI_commons.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <string.h>
#include <chrono>
#include "mpi.h"

using namespace std;

double beta_0 = 0.08; // parametry dla GSO
double rho = 0.4;
double gamma_custom = 0.6;
double s = 1;
double nt = 5.0;
double l_zero = 5.0;
double rs = 250.0;  // the constant radial sensor range

int type = 2, nbytes = 0, EUI_SUCCEED = 0;

double d_distance(int i, int j)
{
    double sum = 0.0;
    for (int xi = 0; xi < dim; xi++)
    {
        sum += pow(gwa[i][xi] - gwa[j][xi], 2);
    }
    return sqrt(sum);
}

void init_gw_population(int lb, int ub)
{
    for (int i = 0; i < population_num; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            double r = thread_local_rand(0, 1);
            gwa[i][j] = r * (ub - lb) + lb;
        }
        L[i] = l_zero;  // Początkowa wielkość lucyferyny
        rd[i] = rs; // Początkowy dystans radialny
    }
}

int current_best_id()
{
    int current_best = 0;
    for (int i = 0; i < population_num; i++)
    {
        if (L[i] > L[current_best])
        {
            current_best = i;
        }
    }
    return current_best;
}

void update_luciferin(scrFunc optimized_problem, int iter)
{
    int k;
    for (k = from; k < to; k++)
    {
        L[k] = (1.0 - rho) * L[k] + gamma_custom * score_func(gwa[k],dim);
    }
}

void set_neighborhood(int i)
{
    N[i][i] = 0;
    for (int j = 0; j < population_num; j++)
    {
        if (j != i)
        {
            if ((L[j] > L[i]) && (d_distance(i, j) < rd[i]))
                N[i][j] = 1;
            else
                N[i][j] = 0;
        }
    }
}

int count_neighbors(int i)
{
    int neighbors = 0;
    for (int j = 0; j < population_num; j++)
        {
            if (N[i][j] == 1){
                neighbors++;
            }
        }

    return neighbors;
}

void probability_fun(int i)
{
    double sum = 0;

    for (int j = 0; j < population_num; j++)
    {
        sum += N[i][j] * max(0.0,(L[j] - L[i]));
    }

    for (int j = 0; j < population_num; j++)
    {
        if(L[j] - L[i] < 0 || sum == 0){
            P[i][j] = 0;
        } else {
            P[i][j] = N[i][j] * (L[j] - L[i]) / sum;
        }
    }
}

int select_glowworm(int i)
{
    double rndNr = thread_local_rand(0.0, 1.0);
    double offset = 0.0;

    for (int j = 0; j < population_num; j++)
    {
        offset += P[i][j];
        if(rndNr < offset){
            return j;
        }
    }
    if(offset != 0)
        cout << "Mistake! Mimo sąsiadów nie przesunięto świetlika!" << endl;
    return i;
}

double clip_x(double x) {
    return min(max(x, lb), ub);
}

void move_glowworm(int i, int j)
{
    if(i == j)
        return;
    double *tmp = new double[dim];
    for (int xi = 0; xi < dim; xi++)
    {
        double var = gwa[j][xi] - gwa[i][xi];
        tmp[xi] = gwa[i][xi] + s * (var / d_distance(i,j));
        gwa[i][xi] = clip_x(tmp[xi]);
    }
}

double clip_solution(double solution)
{
    return min(rs, max(0.0, solution));
}

void update_rd(int i)
{
    rd[i] = clip_solution(rd[i] + beta_0 * (nt - count_neighbors(i)));
}

int runGSO() 
{    
    int it_num = 0;
    int best_id = 0;
    printf("Run gso with params from %d to %d. my ID: %d\n", from, to, mynum);

    double current_best_func_val = 0.0d;
    double last_changed = 0.0d;
    int last_change_it_num = it_num;
    while (it_num < max_it)
    {
        update_luciferin(optimized_problem, it_num);
        best_id = current_best_id();
        int received_best_id = check_for_finish();
        if (received_best_id >= 0) {
            bcast_my_population(gwa, L);
            recv_others_population(gwa, L, false);
            double received_func_val = optimized_problem(gwa[received_best_id], dim);
            if (current_best_func_val > received_func_val ) {
                best_id = received_best_id;
                current_best_func_val = received_func_val;
            }
            break;
        }
        current_best_func_val = optimized_problem(gwa[best_id], dim);
        if (current_best_func_val != last_changed) {
            last_change_it_num = it_num;
            last_changed = current_best_func_val;
        }
        if (end_searching(gwa[best_id], end_condition, end_condition_fun_value, dim) || it_num - last_change_it_num > max_iters_no_change) {
            inform_on_finish(best_id);
            bcast_my_population(gwa, L);
            recv_others_population(gwa, L, false);
            break;
        }

        // movement phase
        int k;
        for (int k = from; k < to; k++)
        {
            // określanie zbioru sąsiadów
            set_neighborhood(k);
            
            // obliczanie prawdopodobieństwa przemieszczenia agenta w kierunku sąsiada j
            probability_fun(k);
            int j = select_glowworm(k);
            move_glowworm(k,j);

            // modyfikacja zakresu radialnego sąsiedztwa
            update_rd(k);
        }
        if (it_num > 0 && it_num % iters_without_sync == 0)
        {
            bcast_my_population(gwa, L);
            recv_others_population(gwa, L, false);
            if (debug_print) {
                print_solution(best_id, gwa[best_id], L[best_id], it_num, dim, optimized_problem(gwa[best_id], dim));
            }
            best_id = current_best_id();
            bool should_end = end_searching(gwa[best_id], end_condition, end_condition_fun_value, dim);
            current_best_func_val = optimized_problem(gwa[best_id], dim);
            if (current_best_func_val != last_changed) {
                last_change_it_num = it_num;
                last_changed = current_best_func_val;
            }
        }
        it_num++;
    }

    if (debug_print) {
        print_solution(best_id, gwa[best_id], L[best_id], it_num, dim, optimized_problem(gwa[best_id], dim));
    }
    printf("Searching ended for process with ID %d with best id %d and its score %f\n", mynum, best_id, current_best_func_val);
	return best_id;
}

double* prepare_population() {
    init_gw_population(lb, ub);
    cout << mynum << ": prepared GSO population." << endl;
    return flatten_2d_array(gwa, 0, population_num, dim);
}

int main(int argc, char** argv)
{
    parseArgs(argc, argv);
    // initialization MPI
    initMPI(argc, argv);
    configure_GSO();

    double *sendrecv_buffer;
    if (mynum==0){
        // init population in root process
        sendrecv_buffer = prepare_population();
    } else {
        sendrecv_buffer = new double[population_num * dim];
    }
    info = MPI_Bcast(&(sendrecv_buffer[0]),population_num * dim, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    if (mynum != 0) {
        // receive initial population in rest of processes
        recv_init_population(gwa, sendrecv_buffer);
        printf(" Received initial population, my ID: %d\n", mynum) ;
    }

    chrono::steady_clock::time_point begin = chrono::steady_clock::now();
    // GSO run - find best for current process
	int best_id = runGSO();
    // exchange solution and print best in root process
    double best_result = exchange_results(best_id, gwa);
    if (mynum == 0) {
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        double measured_time = chrono::duration_cast<chrono::duration<float>>(end - begin).count();
        printf("Found solution %f in: %f s\n", best_result, measured_time);
    }
	MPI_Finalize();

    return 0;
}

