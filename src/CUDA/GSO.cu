#include "common.h"
#include "kernels/GSO_kernels.cuh"
//#include <omp.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <string.h>
#include <chrono>

using namespace std;

curandState *RNGstates;
int blocks, threads_pop, threads_dim;

void configure_GSO()
{


    // ustalenie liczby blokow i watkow
    blocks = population_num;
    threads_dim = 32;
    threads_pop = 32;

    // ustawienie parametrow uruchomienia w GPU
    setParam<<<1,1>>>(population_num,dim,end_condition);
    cudaDeviceSynchronize();
    

    // alokowanie w pamieci tablic zmiennych, modyfikowanych w kernelach
    cudaMallocManaged(&gwa, population_num*sizeof(double*));
    for (int i=0;i<population_num;++i)
        cudaMallocManaged(&gwa[i],dim*sizeof(double));

    cudaMallocManaged(&L, population_num*sizeof(double));
    cudaMallocManaged(&rd, population_num*sizeof(double));

    cudaMallocManaged(&P, population_num*sizeof(double*));
    for (int i=0;i<population_num;++i)
        cudaMallocManaged(&P[i],population_num*sizeof(double));
    cudaMallocManaged(&N, population_num*sizeof(bool*));
    for (int i=0;i<population_num;++i)
        cudaMallocManaged(&N[i],population_num*sizeof(bool));

    // inicjowanie stanow generatora liczb losowych
    cudaMalloc(&RNGstates, blocks*max(threads_dim, threads_pop)*sizeof(curandState));
    int ra = thread_local_rand(0,10000000);
    initStates<<<blocks,max(threads_dim, threads_pop)>>>(RNGstates, ra);
    cudaDeviceSynchronize();

    cout << "CUDA init complete!\n";

}

void freeCUDA()
{
    for (int i=0;i<population_num;++i)
        cudaFree(gwa[i]);
    cudaFree(gwa);
    cudaFree(L);
    cudaFree(rd);
    for (int i=0;i<population_num;++i)
        cudaFree(P[i]);
    cudaFree(P);
    for (int i=0;i<population_num;++i)
        cudaFree(N[i]);
    cudaFree(N);
    cudaFree(RNGstates);
}

int current_best_id()
{
    int current_best = 0;
    for (int i = 0; i < population_num; i++)
    {
        if (L[i] > L[current_best])
        {
            current_best = i;
        }
    }
    return current_best;
}

void runGSO() {
    int it_num = 0;
    int best_id = 0;

    initGwPopulationCUDA<<<blocks,threads_dim>>>(lb,ub,RNGstates,gwa,L,rd);
    cudaDeviceSynchronize();

    //double time_start = omp_get_wtime();
    double current_best_func_val = 0.0;
    double last_changed = 0.0;
    int last_change_it_num = it_num;
    chrono::steady_clock::time_point begin = chrono::steady_clock::now();

    while (it_num < max_it)
    {
        updateLuciferinCUDA<<<blocks,threads_dim>>>(gwa,L);
        cudaDeviceSynchronize();
        setNeighborhoodCUDA<<<blocks, threads_pop>>>(gwa,L,rd,N);
        cudaDeviceSynchronize();
        probabilityCUDA<<<blocks, threads_pop>>>(L,P,N);
        cudaDeviceSynchronize();
        moveGlowwormCUDA<<<blocks, threads_dim>>>(RNGstates,gwa,P);
        updateRdCUDA<<<blocks, threads_pop>>>(rd,N);
        cudaDeviceSynchronize();

        if (it_num % 5 == 0)
        {
            best_id = current_best_id();
            if (end_searching(gwa[best_id], end_condition, end_condition_fun_value, dim) || it_num - last_change_it_num > max_iters_no_change)
                break;
            current_best_func_val = optimized_problem(gwa[best_id], dim);
            if (current_best_func_val != last_changed) {
                last_change_it_num = it_num;
                last_changed = current_best_func_val;
            }
        }
        
        
        if (it_num % 10 == 0 && debug_print)
        {
            print_solution(best_id, gwa[best_id], L[best_id], it_num, dim, optimized_problem(gwa[best_id], dim));
        }
        it_num++;
    }

    //double measured_time = omp_get_wtime() - time_start;
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    double measured_time = chrono::duration_cast<chrono::duration<float>>(end - begin).count();
    if (debug_print)
    {
        print_solution(best_id, gwa[best_id], L[best_id], it_num, dim, optimized_problem(gwa[best_id], dim));
    }
    
    cout << "Searching ended" << endl;
    cout << "Mesaured time: " << measured_time << " seconds" << endl;
}

int main(int argc, char** argv)
{
    parseArgs(argc, argv);
    configure_GSO();
    
    runGSO();
    freeCUDA();

    return 0;
}