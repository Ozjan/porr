#include "common.h"
#include <thread>
#include <random>
using namespace std;

const double MIN_RAND = -0.5;
const double MAX_RAND = 0.5;
const double global_optimum_fun_value = 0.0;

const int lb_fun1 = -40; // lower bound task1
const int ub_fun1 = 40;  // upper bound task1
const int lb_fun2 = -30; // lower bound task2
const int ub_fun2 = 30;  // upper bound task2

double ub;
double lb;
int dim;
int num_threads;
int population_num;
double end_condition;
double end_condition_fun_value;
bool debug_print;
int max_iters_no_change;
int max_it;
double** ffa;   // firefly agents
double* I;      // light brightness

double** gwa;
double* L;      // luciferin
double* rd;     // radial distance
double** P;     //probability matrix
bool** N;     // neighborhood matrix
scrFunc optimized_problem;
scrFunc score_func;

double thread_local_rand(double min, double max)
{
    mt19937 rng_mt(std::time(nullptr));
    thread_local mt19937 generator(rng_mt);
    uniform_real_distribution<double> distribution(min, max);
    return distribution(generator);
}

//pobiera wektor współrzędnych najlepszego świetlika
bool end_searching(double* best_xi, double end_condition, double end_condition_fun_value, int dim)
{
    double sum = 0;
    for (int i = 0; i < dim; i++)
    {
        sum += pow(best_xi[i], 2);
    }
    double fun_value=optimized_problem(best_xi, dim);

    if (sqrt(sum) <= end_condition) {
        cout << "end on location condition" << endl;
        return true;
    }
    else if (fun_value - global_optimum_fun_value <= end_condition_fun_value) {
        cout << "end on fun value" << endl;
        return true;
    } else {
        return false;
    }
}

void print_solution(int id, double* xi, double intensity, int it_num, int dim)
{
    cout << "Iteration " << it_num << ": ";
    cout << "Best FFA " << id << " ( ";
    cout << ") intensity = " << intensity << endl;
}

//wypisuje nr iteracji, id świetlika, współrzędne i intensywność
void print_solution(int id, double* xi, double intensity, int it_num, int dim, double functionValue)
{
    cout << "Iteration " << it_num << ": ";
    cout << "Best agent " << id;
    cout << ", intensity/luciferin = " << intensity << " function value: " << functionValue <<   endl;
}

// optimization tasks
double fun1(double* x_vec, int dim)
{
    double sum = 0.0;
    double product = 1.0;

    for (int i = 0; i < dim; i++)
    {
        sum += pow(x_vec[i], 2);
        product *= cos(x_vec[i] / (i + 1));
    }
    return sum / 40 + 1 - product;
    
}

double fun2(double* x_vec, int dim)
{
    double sum1 = 0.0;
    double sum2 = 0.0;

    for (int i = 0; i < dim; i++)
    {
        sum1 += pow(x_vec[i], 2);
        sum2 += cos(2 * M_PI * x_vec[i]);
    }
    return -20 * exp(-0.2 * sqrt(sum1 / dim )) - exp(sum2 / dim ) + 20 + M_E;
}

void parseArgs(int argc, char** argv) {
    if (argc < 6) {
        cout << "Too litle args: " << argc;
        throw new exception();
    }
    string function_name = argv[1];
    if (function_name == "fun1"){
        ub = ub_fun1;
        lb = lb_fun1;
        setFn<<<1,1>>>(1);
        optimized_problem = fun1;
    } else if (function_name == "fun2"){
        ub = ub_fun2;
        lb = lb_fun2;
        setFn<<<1,1>>>(2);
        optimized_problem = fun2;
    } else {
        cout << "wrong function name";
        throw new exception();
    }

    dim = atoi(argv[2]);
    population_num = atoi(argv[3]);
    max_it = atoi(argv[4]);
    end_condition = atof(argv[5]);
    end_condition_fun_value = atof(argv[6]);

    if (argc > 7) {
        max_iters_no_change = atoi(argv[7]);
    } else {
        max_iters_no_change = 40;
    }
    string temp = argv[8];
    if (argc > 8 && temp == "true") {
        debug_print = true;
    } else {
        debug_print = false;
    }
    if (debug_print) {
        cout << "Run with parameters: " << endl 
                << "function name -> " << function_name << endl
                << "dimension -> " << dim << endl
                << "population number -> " << population_num << endl
                << "Max iterations -> " << max_it << endl
                << "End condition -> " << end_condition << endl
                << "Max number of iterations without change -> " << max_iters_no_change << endl;
    }
}



