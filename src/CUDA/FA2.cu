#include "common.h"
#include "kernels/FA_kernels.cuh"
//#include <omp.h>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <string.h>
#include <chrono>

using namespace std;

double beta0 = 0.5; // beta parameter
double alpha = 0.7;
double gammaParam = 0.05; // gamma parameter
double search_range = 1; // ub_fun1 - lb_fun1;

// zmienna pomocnicza do CUDA
curandStatePhilox4_32_10_t *RNGstates;
bool* end_s;
double** temp_ffa;
int blocks, threads;

void CUDAinit()
{
    blocks = population_num;
    threads = 32;
    
    cudaMallocManaged(&end_s, sizeof(bool));
    *end_s = false;

    cudaMallocManaged(&I, population_num*sizeof(double));
    cudaMallocManaged(&ffa, population_num*sizeof(double*));
    cudaMallocManaged(&temp_ffa, population_num*sizeof(double*));
    for (int i=0;i<population_num;++i)
    {
        cudaMallocManaged(&ffa[i], dim*sizeof(double));
        cudaMallocManaged(&temp_ffa[i], dim*sizeof(double));
    }

    cudaMalloc(&RNGstates, blocks*threads*sizeof(curandStatePhilox4_32_10_t));

    int ra = thread_local_rand(0,10000000);
    setParam<<<1,1>>>(population_num, dim, end_condition, ub, lb);
    initStates<<<blocks,threads>>>(RNGstates, ra);
    cudaDeviceSynchronize();

        
    cout << "CUDA init complete\n";
}

void CUDAfree()
{
    cudaFree(end_s);
    cudaFree(I);
    for (int i = 0; i<dim; i++){
        cudaFree(ffa[i]);
        cudaFree(temp_ffa[i]);
    }
    cudaFree(ffa);
    cudaFree(temp_ffa);
}


void init_fa_population(double lb, double ub)
{
    for (int i = 0; i < population_num; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            double r = thread_local_rand(0, 1);
            ffa[i][j] = r * (ub - lb) + lb;
        }
        I[i] = 1.0;
    }
}

int current_best_id()
{
        int current_best = 0;
        for (int i = 0; i < population_num; i++)
        {
            if (I[i] > I[current_best]) {
                current_best = i;
            }
        }
    return current_best;
}

void runFA() {
    init_fa_population(lb, ub);

    dim3 blocks2D(population_num,population_num);
    int threads = 32;

    double current_best_func_val = 0.0;
    double last_changed = 0.0;
    int it_num = 0;
    int last_change_it_num = it_num;
    int best_id = 0;
    chrono::steady_clock::time_point begin = chrono::steady_clock::now();
    while (it_num < max_it)
    {
        updateIntensitiesCUDA<<<blocks, threads>>>(ffa, I);
        cudaDeviceSynchronize();
        moveFAPulledCUDA<<<blocks2D, threads>>>(ffa,I);
        cudaDeviceSynchronize();
        moveFARandomCUDA<<<blocks, threads>>>(RNGstates,ffa,temp_ffa,I);
        cudaDeviceSynchronize();
        
        if (it_num % 5 == 0)
        {
            current_best_func_val = optimized_problem(ffa[best_id], dim);
            if (current_best_func_val != last_changed) {
                last_change_it_num = it_num;
                last_changed = current_best_func_val;
            }
            best_id = current_best_id();
            if (end_searching(ffa[best_id], end_condition,end_condition_fun_value, dim) || it_num - last_change_it_num > max_iters_no_change)
                break;
        }
        
        

        if (it_num % 10 == 0 && debug_print)
            print_solution(best_id, ffa[best_id], I[best_id], it_num, dim, optimized_problem(ffa[best_id], dim));
        it_num++;
    }

    //double measured_time = omp_get_wtime() - time_start;
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    double measured_time = chrono::duration_cast<chrono::duration<float>>(end - begin).count();
    if (debug_print) {
        print_solution(best_id, ffa[best_id], I[best_id], it_num, dim, optimized_problem(ffa[best_id], dim));

        
    }
    cout << "Searching ended" << endl;
    cout << "Mesaured time: " << measured_time << " seconds\n";
}

int main(int argc, char** argv)
{
    parseArgs(argc, argv);
    CUDAinit();
 
    runFA();
    
    CUDAfree();
    return 0;
}
