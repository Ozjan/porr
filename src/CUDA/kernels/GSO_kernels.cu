#include "GSO_kernels.cuh"

typedef double (*fun)(double*);
__device__ fun func;

// parametry uruchomienia
__device__ int dimC;
__device__ int popC;
__device__ double endC;

// parametry GSO
__device__ const double betaC = 0.08;
__device__ const double rhoC = 0.4;
__device__ const double gammaC = 0.6;
__device__ const double sC = 1;
__device__ const double ntC = 5.0;
__device__ const double l_zero = 5.0;
__device__ const double rs = 125.0;  //the constant radial sensor range

// globalne (device) zmienne pomocnicze
__device__ int bestID;
__device__ double bestL;


// *****************************
// funkcje ogolnego zastosowania
// *****************************

__device__ double sumReduction(double sum)
{
    double value = 0.0;
    for (int i=16; i>=1; i/=2)
        value += __shfl_xor_sync(0xffffffff, sum, i, 32);

    return value;
}

__global__ void initStates(curandState* states, int seed)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    curand_init(seed,12345*index,0,&states[index]);
}

__global__ void setParam(int population_num, int dim, double end)
{
    dimC = dim;
    popC = population_num;
    endC = end;
    bestID = 0;
    bestL = 0;
}

__global__ void setFn(int function)
{
    if (function == 1)
        func = fun1CUDA;
    else if (function == 2)
        func = fun2CUDA;

    return;
}

__device__ double fun1CUDA(double* x_vec)
{
    double sum = 0.0f;
    double product = 1.0f;
    
    
    for (int i = threadIdx.x; i < dimC; i+=blockDim.x)
    {
        sum += pow(x_vec[i], 2);    
        product *= cos(x_vec[i] / (i + 1));
    }
    __syncthreads();

    for (int i=16; i>=1; i/=2) {
        sum += __shfl_xor_sync(0xffffffff, sum, i, 32);
        product *= __shfl_xor_sync(0xffffffff, product, i, 32);
    }
    return sum/40.0 + 1.0 - product;
    
}

__device__ double fun2CUDA(double* x_vec)
{
    double sum1 = 0.0f;
    double sum2 = 0.0f;
    
    
    for (int i = 0; i < dimC; i++)
    {
        sum1 += pow(x_vec[i], 2);
        sum2 += cos(2 * M_PI * x_vec[i]);
    }
    __syncthreads();

    sum1 = sumReduction(sum1);
    sum2 = sumReduction(sum2);
    
    return -20 * exp(-0.2 * sqrt(sum1 / dimC )) - exp(sum2 / dimC ) + 20 + M_E;
    
}

// **************
// kernele do GSO
// **************

// <<<blocks,threads_dim>>>
__global__ void initGwPopulationCUDA(int lb, int ub, curandState* states, double** gwa, double* L, double* rd)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    curandState state_local = states[index];
    for (int i = blockIdx.x; i < popC; i+=gridDim.x)
    {
        for (int j = threadIdx.x; j < dimC; j+=blockDim.x)
        {
            gwa[i][j] = curand_uniform(&state_local) * (ub - lb) + lb;
        }
        if (threadIdx.x == 0)
        {
            L[i] = l_zero;  // Początkowa wielkość lucyferyny
            rd[i] = rs; // Początkowy dystans radialny
        }
    }
    states[index] = state_local;
}

// <<<blocks,threads_dim>>>
__global__ void updateLuciferinCUDA(double** gwa, double* L)
{
    for (int k = blockIdx.x; k < popC; k+=gridDim.x)
    {
        L[k] = (1.0 - rhoC) * L[k] + gammaC * 1.0/func(gwa[k]);
    }
}

// <<<blocks, threads_pop>>> (potencjalnie <<<dim3(blocks,blocks), threads_pop>>>)
__global__ void setNeighborhoodCUDA(double** gwa, double* L, double* rd, bool** N)
{
    for (int i = blockIdx.x; i < popC; i+=gridDim.x)
    {
        N[i][i] = 0;
        for (int j = threadIdx.x; j < popC; j+=blockDim.x)
        {
            if ((L[j] > L[i]) && (distance(gwa, i, j) < rd[i]))
                N[i][j] = 1;
            else
                N[i][j] = 0;
        }
    }
}

// <<<blocks, threads_pop>>>
__global__ void probabilityCUDA(double* L, double** P, bool** N)
{
    for (int i = blockIdx.x; i < popC; i+=gridDim.x)
    {
        double sum = 0.0;

        for (int j = threadIdx.x; j < popC; j+=blockDim.x)
            sum += float(N[i][j]) * max(0.0,(L[j] - L[i]));

        __syncthreads();
        sum = sumReduction(sum);

        for (int j = threadIdx.x; j < popC; j+=blockDim.x)
        {
            if(L[j] - L[i] < 0 || sum == 0.0)
                P[i][j] = 0;
            else
                P[i][j] = float(N[i][j]) * (L[j] - L[i]) / sum;
        }
    }
}

// <<<blocks, threads_dim>>> (potencjalnie <<<(blocks,blocks), threads_dim>>>)
__global__ void moveGlowwormCUDA(curandState* states, double** gwa, double** P)
{
    int j = 0;
    for (int i = blockIdx.x; i < popC; i+=gridDim.x)
    {
        if (threadIdx.x == 0)
            j = selectGlowworm(states,P,i);
        j = __shfl_sync(0xffffffff, j, 0);

        if(i == j)
            return;

        for (int xi = threadIdx.x; xi < dimC; xi+=blockDim.x)
            gwa[i][xi] = gwa[i][xi] + sC * ((gwa[j][xi] - gwa[i][xi]) / distance(gwa,i,j));
    }
}



// <<<blocks, threads_pop>>>
__global__ void updateRdCUDA(double* rd, bool** N)
{
    for (int i = blockIdx.x; i < popC; i+=gridDim.x)
        rd[i] = rd[i] + betaC * (ntC - countNeighbors(N,i));
}

// **********************
// funkcje dla GPU do GSO
// **********************

// do setNeighborhood() i moveGlowworm()
__device__ double distance(double** gwa, int i, int j)
{
    double sum = 0.0;
    for (int xi = 0; xi < dimC; xi++)
    {
        sum += pow(gwa[i][xi] - gwa[j][xi], 2);
    }
    return sqrt(sum);
}

// do moveGlowworm()
__device__ int selectGlowworm(curandState* states, double** P, int i)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    double rndNr = curand_uniform(&states[index]);
    double offset = 0.0;

    for (int j = 0; j < popC; j++)
    {
        offset += P[i][j];
        if(rndNr < offset){
            return j;
        }
    }
    if(offset != 0)
        printf("Error! Agent did not move!\n");
    return i;
}

__device__ int countNeighbors(bool** N, int i)
{
    int neighbors = 0;
    for (int j = threadIdx.x; j < popC; j+=blockDim.x)
        if (N[i][j])
            neighbors++;

    return sumReduction(neighbors);
}