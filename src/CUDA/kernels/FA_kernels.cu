#include "FA_kernels.cuh"

typedef double (*fun)(double*);
__device__ fun func;

// parametry uruchomienia
__device__ int dimC;
__device__ int popC;
__device__ double endC;
__device__ double ubC;
__device__ double lbC;

__device__ double betaC = 0.2; // beta parameter
__device__ double alphaC = 0.7;
__device__ double gammaC; // gamma parameter
__device__ double search_range = 1;

// globalne (device) zmienne pomocnicze
__device__ int bestID;
__device__ double bestL;


// <<<blocks,threads>>>
__global__ void updateIntensitiesCUDA(double** ffa, double* I)
{
    double value = 0.0;
    for (int i = blockIdx.x; i < popC; i+=gridDim.x)
    {
        value = func(ffa[i]);
        __syncthreads();
        if (threadIdx.x == 0){
            I[i] = 1.0/value;
        }
    }
}

// <<<dim3(blocks,blocks),threads>>>
__global__ void moveFAPulledCUDA(double** ffa, double* I)
{
    for (int i = blockIdx.y; i < popC; i+=gridDim.y)
    {
        for (int j = blockIdx.x; j < popC; j+=gridDim.x)
        {
            if (I[j] > I[i])
            {
                double dist = distance(ffa, i, j);
                double beta = betaC * exp(-gammaC * pow(dist, 2));
                for (int k = threadIdx.x; k < dimC; k+=blockDim.x)
                {
                    ffa[i][k] += beta * (ffa[j][k] - ffa[i][k]);
                    ffa[i][k] = clipSolution(ffa[i][k]);
                }
            }
        }
    }
}

// <<<blocks,threads>>>
__global__ void moveFARandomCUDA(curandStatePhilox4_32_10_t* states, double** ffa, double** tmp, double* I)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    curandStatePhilox4_32_10_t state_local = states[index];

    for (int i = blockIdx.x; i < popC; i+=gridDim.x)
    {
        for (int j = threadIdx.x; j < dimC; j+=blockDim.x)
            tmp[i][j] = clipSolution(ffa[i][j] + alphaC * (curand_uniform(&state_local)-0.5));

        __syncthreads();
        double I_tmp = 1.0 / func(tmp[i]);
        __syncthreads();
        if (I_tmp > I[i])
        {
            for (int k = threadIdx.x; k < dimC; k+=blockDim.x)
            {
                ffa[i][k] = tmp[i][k];
            }
        }
    }
    states[index] = state_local;
}

// ************************
// funkcje wewnatrz kerneli
// ************************

__device__ double distance(double** ffa, int i, int j)
{
    double sum = 0.0;
    for (int xi = threadIdx.x; xi < dimC; xi+=blockDim.x)
        sum += pow(ffa[i][xi] - ffa[j][xi], 2);
    
    sum = sumReduction(sum);
    
    return sqrt(sum);
}


// *****************************
// funkcje ogolnego zastosowania
// *****************************

__device__ double clipSolution(double solution)
{
    return max(min(solution, ubC), lbC);
}

__device__ double sumReduction(double sum)
{
    // double value = 0.0;
    for (int i=16; i>=1; i/=2)
        sum += __shfl_xor_sync(0xffffffff, sum, i, 32);

    __syncthreads();

    return sum;
}

__global__ void initStates(curandStatePhilox4_32_10_t* states, int seed)
{
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    curand_init(seed,index,0,&states[index]);
}

__global__ void setParam(int population_num, int dim, double end, double ub, double lb)
{
    dimC = dim;
    popC = population_num;
    endC = end;
    ubC = ub;
    lbC = lb;
    bestID = 0;
    bestL = 0;
    gammaC = 1.0/sqrt(dimC*(ubC-lbC));
}

__global__ void setFn(int function)
{
    if (function == 1)
        func = fun1CUDA;
    else if (function == 2)
        func = fun2CUDA;

    return;
}

__device__ double fun1CUDA(double* x_vec)
{
    double sum = 0.0f;
    double product = 1.0f;
    
    
    for (int i = threadIdx.x; i < dimC; i+=blockDim.x)
    {
        sum += pow(x_vec[i], 2);    
        product *= cos(x_vec[i] / (i + 1));
    }
    __syncthreads();

    for (int i=16; i>=1; i/=2) {
        sum += __shfl_xor_sync(0xffffffff, sum, i, 32);
        product *= __shfl_xor_sync(0xffffffff, product, i, 32);
    }
    return sum/40.0 + 1.0 - product;
    
}

__device__ double fun2CUDA(double* x_vec)
{
    double sum1 = 0.0f;
    double sum2 = 0.0f;
    
    
    for (int i = threadIdx.x; i < dimC; i+=blockDim.x)
    {
        sum1 += pow(x_vec[i], 2);
        sum2 += cos(2 * M_PI * x_vec[i]);
    }
    __syncthreads();

    for (int i=16; i>=1; i/=2){
        sum1 += __shfl_xor_sync(0xffffffff, sum1, i, 32);
        __syncthreads();
        sum2 += __shfl_xor_sync(0xffffffff, sum2, i, 32);
    }
    
    return -20 * exp(-0.2 * sqrt(sum1 / dimC )) - exp(sum2 / dimC ) + 20 + M_E;
    
}