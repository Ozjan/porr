#pragma once

#include "../common.h"
#include <curand_kernel.h>


// inicjalizacja populacji na GPU
__global__ void initGwPopulationCUDA(int lb, int ub, curandState* states, double** gwa, double* L, double* rd);

// kernele wewnatrz iteracji
__global__ void updateLuciferinCUDA(double** gwa, double* L);
__global__ void setNeighborhoodCUDA(double** gwa, double* L, double* rd, bool** N);
__global__ void probabilityCUDA(double* L, double** P, bool** N);
__global__ void moveGlowwormCUDA(curandState* states, double** gwa, double** P);
__global__ void updateRdCUDA(double* rd, bool** N);

// funckja do setNeighborhood()
__device__ double distance(double** gwa, int i, int j);

// funkcja do moveGlowworm()
__device__ int selectGlowworm(curandState* states, double** P, int i);

// funkcja do updateRd()
__device__ int countNeighbors(bool** N, int i);


// funkcje do optymalizacji
__device__ double fun1CUDA(double* x_vec);
__device__ double fun2CUDA(double* x_vec);

// funkcje pomocnicze ogolnego zastosowania
__global__ void initStates(curandState* states, int seed);
__global__ void setParam(int population_num, int dim, double end);
__global__ void setFn(int function);
__device__ double sumReduction(double sum);
