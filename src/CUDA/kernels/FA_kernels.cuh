#pragma once

#include "../common.h"
#include <curand_kernel.h>

// (opcjonalnie) inicjalizacja populacji
// __global__ void initFAPopulation();

// kernele wewnatrz iteracji
__global__ void updateIntensitiesCUDA(double** ffa, double* I);
__global__ void moveFAPulledCUDA(double** ffa, double* I);
__global__ void moveFARandomCUDA(curandStatePhilox4_32_10_t* states, double** ffa, double** tmp, double* I);


// funkcja liczaca odleglosc
__device__ double distance(double** ffa, int i, int j);


// funkcje do optymalizacji
__device__ double fun1CUDA(double* x_vec);
__device__ double fun2CUDA(double* x_vec);

// funkcje pomocnicze ogolnego zastosowania
__device__ double clipSolution(double solution);
__device__ double sumReduction(double sum);
__global__ void initStates(curandStatePhilox4_32_10_t* states, int seed);
__global__ void setParam(int population_num, int dim, double end, double ub, double lb);
__global__ void setFn(int function);
