#include "common.h"
#include <thread>
#include <random>
using namespace std;

double thread_local_rand(double min, double max)
{
    mt19937 rng_mt(std::time(nullptr));
    thread_local mt19937 generator(rng_mt);
    uniform_real_distribution<double> distribution(min, max);
    return distribution(generator);
}

//pobiera wektor współrzędnych najlepszego świetlika
bool end_searching(double* best_xi)
{
    double sum = 0;
    int i;

    // # pragma omp parallel
    // {
    //     # pragma omp for shared (best_xi,dim) private (i) reduction (+: sum )
    //     {
            for (i =0; i<dim; i++){
                sum += pow(best_xi[i], 2);
            }
    //     }
    // }

    if (sqrt(sum) <= end_condition)
        return true;
    else
        return false;
    
}

//wypisuje nr iteracji, id świetlika, współrzędne i intensywność
void print_solution(int id, double* xi, double intensity, int it_num)
{
    cout << "Iteration " << it_num << ": ";
    cout << "Best FFA " << id << " ( ";
    for (int i = 0; i < dim; i++)
        cout << xi[i] << " ";
    cout << ") intensity = " << intensity << endl;
}

// optimization tasks
double fun1(double* x_vec)
{
    double sum = 0.0;
    double product = 1.0;
    int i;

    // # pragma omp parallel
    // {
    //     # pragma omp for shared (x_vec,dim) private (i) reduction (+: sum ) reduction (*: product )
    //     {
            for (i =0; i<dim; i++){
                sum += pow(x_vec[i], 2);
                product *= cos(x_vec[i] / (i + 1));
            }
    //     }
    // }
    return sum / 40 + 1 - product;
}

double fun2(double* x_vec)
{
    double sum1 = 0.0;
    double sum2 = 0.0;
    int i;
    // # pragma omp parallel
    // {
    //     # pragma omp for shared (x_vec,dim) private (i) reduction (+: sum1 ) reduction (+: sum2 )
    //     {
            for (i =0; i<dim; i++){
                sum1 += pow(x_vec[i], 2);
                sum2 += cos(2 * M_PI * x_vec[i]);
            }
    //     }
    // }
    return 20 * exp(-0.2 * sqrt(sum1 / dim )) - exp(sum2 / dim ) + 20 + M_E;
}