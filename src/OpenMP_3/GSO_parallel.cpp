#include "../common.h"
#include <omp.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <string.h>
#include <chrono>

using namespace std;

double beta_0 = 0.08;
double rho = 0.4;
double gamma_custom = 0.6;
double s = 1;
double nt = 5.0;
double l_zero = 5.0;
double rs = 250.0; 

double d_distance(int i, int j)
{
    double sum = 0.0;
    for (int xi = 0; xi < dim; xi++)
    {
        sum += pow(gwa[i][xi] - gwa[j][xi], 2);
    }
    return sqrt(sum);
}

void init_gw_population(int lb, int ub)
{
    for (int i = 0; i < population_num; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            double r = ((double)rand() / ((double)(RAND_MAX) + (double)(1)));
            gwa[i][j] = r * (ub - lb) + lb;
        }
        L[i] = l_zero;  // Początkowa wielkość lucyferyny
        rd[i] = rs; // Początkowy dystans radialny
    }
}

int current_best_id()
{
    int current_best = 0;
    for (int i = 0; i < population_num; i++)
    {
        if (L[i] > L[current_best])
        {
            current_best = i;
        }
    }
    return current_best;
}

void update_luciferin()
{
    double L_tmp;
    int k;
    #pragma omp parallel for private (k) shared (population_num, gamma_custom, score_func, gwa, dim, L, rho)
    for (k = 0; k < population_num; k++)
    {
        L[k] = (1.0 - rho) * L[k] + gamma_custom * score_func(gwa[k],dim);
    }
}

void set_neighborhood(int i)
{
    N[i][i] = 0;
    for (int j = 0; j < population_num; j++)
    {
        if (j != i)
        {
            if ((L[j] > L[i]) && (d_distance(i, j) < rd[i]))
                N[i][j] = 1;
            else
                N[i][j] = 0;
        }
    }
}

int count_neighbors(int i)
{
    int neighbors = 0;
    for (int j = 0; j < population_num; j++)
        {
            if (N[i][j] == 1){
                neighbors++;
            }
        }

    return neighbors;
}

void probability_fun(int i)
{
    double sum = 0;

    for (int j = 0; j < population_num; j++)
    {
        sum += N[i][j] * max(0.0,(L[j] - L[i]));
    }

    for (int j = 0; j < population_num; j++)
    {
        if(L[j] - L[i] < 0 || sum == 0){
            P[i][j] = 0;
        } else {
            P[i][j] = N[i][j] * (L[j] - L[i]) / sum;
        }
    }
}

int select_glowworm(int i)
{
    double rndNr = thread_local_rand(0.0, 1.0);
    double offset = 0.0;

    for (int j = 0; j < population_num; j++)
    {
        offset += P[i][j];
        if(rndNr < offset){
            return j;
        }
    }
    if(offset != 0)
        cout << "Mistake! Mimo sąsiadów nie przesunięto świetlika!" << endl;
    return i;
}

double clip_x(double x) {
    return min(max(x, lb), ub);
}

void move_glowworm(int i, int j)
{
    if(i == j)
        return;

    double *tmp = new double[dim];
    for (int xi = 0; xi < dim; xi++)
    {
        double var = gwa[j][xi] - gwa[i][xi];
        tmp[xi] = gwa[i][xi] + s * (var / d_distance(i,j));
        gwa[i][xi] = clip_x(tmp[xi]);
    }
}

double clip_solution(double solution)
{
    return min(rs, max(0.0, solution));
}

void update_rd(int i)
{
    rd[i] = clip_solution(rd[i] + beta_0 * (nt - count_neighbors(i)));
}

void runGSO() {

    fstream file;
    if (dim==2){   
        file.open("GSO_fun1_seq_dim2_iterations.txt",ios::out);
    }
    
    int it_num = 0;
    int best_id = 0;

    for (int i = 0; i < population_num; i++)
    {
        gwa[i] = new double[dim];  
        P[i] = new double[population_num]; 
        N[i] = new double[population_num];    
    }

    init_gw_population(lb, ub);

    double current_best_func_val = 0.0d;
    double last_changed = 0.0d;
    int last_change_it_num = it_num;

    double time_start = omp_get_wtime();

    while (it_num < max_it)
    {
        update_luciferin();
        best_id = current_best_id();
        if (end_searching(gwa[best_id], end_condition, end_condition_fun_value, dim)|| it_num - last_change_it_num > max_iters_no_change)
            break;
        current_best_func_val = optimized_problem(gwa[best_id], dim);
        if (current_best_func_val != last_changed) {
            last_change_it_num = it_num;
            last_changed = current_best_func_val;
        }

        int k;
        #pragma omp parallel for private(k) shared(population_num)
        for (int k = 0; k < population_num; k++)
        {
            set_neighborhood(k);
            
            probability_fun(k);
            int j = select_glowworm(k);
            move_glowworm(k,j);

            update_rd(k);
        }
        if (it_num % 10 == 0 && debug_print)
        {
            print_solution(best_id, gwa[best_id], L[best_id], it_num, dim, optimized_problem(gwa[best_id], dim));
        }
        it_num++;
        if (dim==2){
            file << gwa[best_id][0]<<" "<< gwa[best_id][1]<<" " <<optimized_problem(gwa[best_id], dim)<<endl;
        }
    }

    double measured_time = omp_get_wtime() - time_start;
    if (debug_print)
    {
        print_solution(best_id, gwa[best_id], L[best_id], it_num, dim, optimized_problem(gwa[best_id], dim));
    }
    
    cout << "Searching ended" << endl;
    cout << "Mesaured time: " << measured_time << " seconds" << endl;

    if (dim==2){
        file.close();
    }
}

int main(int argc, char** argv)
{
    parseArgs(argc, argv);
    configure_GSO();
    omp_set_num_threads(num_threads);
    runGSO();
    return 0;
}