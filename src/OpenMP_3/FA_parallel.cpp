#include "../common.h"
#include <omp.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <time.h>
#include <string.h>
#include <chrono>

using namespace std;

double beta0 = 0.2; // beta parameter
double alpha = 0.7;
double gammaParam; // gamma parameter

double clip_solution(double solution)
{
    return max(min(solution, ub), lb);
}

void update_intensities(scrFunc optimized_problem)
{
    int k;
      
    #pragma omp parallel for shared (I, population_num, optimized_problem, ffa, dim) private (k)      
    for (k = 0; k < population_num; k++){
        I[k] = 1.0 / optimized_problem(ffa[k], dim);
    }
 
}

double distance(int i, int j)
{
    double sum = 0.0;
    int xi;

    #pragma omp parallel for shared (ffa, dim, i, j) private (xi) reduction (+: sum)
        for (xi = 0; xi < dim; xi++)
        {
            sum += pow(ffa[i][xi] - ffa[j][xi], 2);
        }

    return sqrt(sum);
}

void update_solution_for_pulled(int pulled, int pulled_by, scrFunc optimized_problem)
{
    double dist = distance(pulled, pulled_by);
    double beta = beta0 * exp(-gammaParam * pow(dist, 2));
    double *tmp = new double[dim];
    int dimension;

    for (dimension = 0; dimension < dim; dimension++)
    {
        double a1 = ffa[pulled][dimension] + beta * (ffa[pulled_by][dimension] - ffa[pulled][dimension]);
        double a2 = alpha * (thread_local_rand(MIN_RAND, MAX_RAND));
        tmp[dimension] = clip_solution(a1 + a2);
    }
    
    if (1.0 / optimized_problem(tmp, dim) > I[pulled])
    {
        int dimension;
        for (dimension = 0; dimension < dim; dimension++)
        {
            ffa[pulled][dimension] = tmp[dimension];
        }
    }
    
    delete tmp;
}

void update_solution_for_best(int best, scrFunc optimized_problem)
{
    double *tmp = new double[dim];
    int dimension;
    #pragma omp parallel for private(dimension) shared(tmp, best, ffa, dim, alpha, MIN_RAND, MAX_RAND)
        for (dimension = 0; dimension < dim; dimension++)
        {
            tmp[dimension] = clip_solution(ffa[best][dimension] + alpha * thread_local_rand(MIN_RAND, MAX_RAND));
        }

    if (1.0 / optimized_problem(tmp, dim) > I[best])
    {
        dimension=0;
        #pragma omp parallel for private(dimension) shared(tmp, best, ffa, dim)
            for (dimension = 0; dimension < dim; dimension++)
            {
                ffa[best][dimension] = tmp[dimension];
            }
    }
    delete tmp;
}

void init_fa_population(double lb, double ub)
{
    for (int i = 0; i < population_num; i++)
    {
        for (int j = 0; j < dim; j++)
        {
            double r = thread_local_rand(0, 1);
            ffa[i][j] = r * (ub - lb) + lb;
        }
        I[i] = 1.0;
    }
}

int current_best_id()
{
        int current_best = 0;
        int i;
        #pragma omp parallel for private(i) shared(I, current_best)
        for (i = 0; i < population_num; i++)
        {
            if (I[i] > I[current_best]) {
                #pragma omp atomic write
                current_best = i;
            }
        }
    return current_best;
}

void runFA() {

    fstream file;
    if (dim==2){   
        file.open("FA_fun1_seq_dim2_iterations.txt",ios::out);
    }

    int best_id = 0;
    int it_num = 0;

    for (int i = 0; i < population_num; i++)
    {
        ffa[i] = new double[dim];
    }
    init_fa_population(lb, ub);

    int current_best = 0;
    double time_start = omp_get_wtime();
    double current_best_func_val = 0.0d;
    double last_changed = 0.0d;
    int last_change_it_num = it_num;
    while (it_num < max_it)
    {
        update_intensities(optimized_problem);
        best_id = current_best_id();
        if (end_searching(ffa[best_id], end_condition, end_condition_fun_value, dim) || it_num - last_change_it_num > max_iters_no_change)
            break;

        current_best_func_val = optimized_problem(ffa[best_id], dim);
        if (current_best_func_val != last_changed) {
            last_change_it_num = it_num;
            last_changed = current_best_func_val;
        }
        int i,j;
        #pragma omp parallel for private(i,j) shared(population_num, I, optimized_problem)
            for (i = 0; i < population_num; i++)
            {
                for (j = 0; j < population_num; j++)
                {
                    if (I[j] > I[i]) {
                        update_solution_for_pulled(i, j, optimized_problem);
                    }
                }
            }
        
        update_solution_for_best(best_id, optimized_problem);
        if (it_num % 10 == 0 && debug_print)
        {
            print_solution(best_id, ffa[best_id], I[best_id], it_num, dim, optimized_problem(ffa[best_id], dim));
        }
        it_num++;

        if (dim==2){
            file << ffa[best_id][0]<<" "<< ffa[best_id][1]<<" " <<optimized_problem(ffa[best_id], dim)<<endl;
        }
    }

    double measured_time = omp_get_wtime() - time_start;
    if (debug_print) {
        print_solution(best_id, ffa[best_id], I[best_id], it_num, dim, optimized_problem(ffa[best_id], dim));
    }
    cout << "Searching ended" << endl;
    cout << "Mesaured time: " << measured_time << " seconds";

    if (dim==2){
        file.close();
    }
}

int main(int argc, char** argv)
{
    parseArgs(argc, argv);
    configure_FA();
    gammaParam = 1.0/sqrt(dim*(ub-lb)); 
    omp_set_num_threads(num_threads);
    runFA();
    return 0;
}
